package com.mx.banorte.services.mo;

public class Message {
    
private String mensaje;
private String error;

public Message(){

}

public Message(String mensaje, String error) {
    this.mensaje = mensaje;
    this.error = error;
}

public String getMensaje() {
    return mensaje;
}

public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
}

public String getError() {
    return error;
}

public void setError(String error) {
    this.error = error;
}








}
